import React, { Component } from 'react'
import { connect } from 'react-redux'
import { upDownQty } from './redux/action/shoeAction'

class cartShoe extends Component {
    renderTBody = () => {
        // return .map để trả về ojb
        return this.props.carts.map((item, index) => {
            return <tr key={index}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td><img style={{ width: "50px" }} src={item.image} alt="" /></td>
                <td>
                    <button onClick={() => { this.props.handleUpDownQty(item.id, -1) }} className='btn btn-primary'>-</button>
                    <span className='mx-3'>{item.qty}</span>
                    <button onClick={() => { this.props.handleUpDownQty(item.id, 1) }} className='btn btn-danger'>+</button>
                </td>
                <td>{item.price * item.qty}$</td>
            </tr>

        })
    }

    render() {
        return (
            <table className='table' >
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Img</td>
                        <td>Qty</td>
                        <td>Price</td>
                    </tr>
                </thead>
                <thead>
                    {this.renderTBody()}
                </thead>
            </table >
        )
    }
}

let mapStateToProp = (state) => {
    return {
        carts: state.shoeReducer.cart
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleUpDownQty: (id, number) => {
            dispatch(upDownQty(id, number))
        }
    }
}

export default connect(mapStateToProp, mapDispatchToProps)(cartShoe);
