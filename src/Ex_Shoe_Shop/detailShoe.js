import React, { Component } from 'react'
import { connect } from 'react-redux'

class detailShoe extends Component {
    render() {
        let { id, name, image, description, price } = this.props.detail;
        return (
            <div className='row alert-secondary text-left mt-5 p-5'>
                <img src={image} alt="" className='col-3' />
                <div className='col-9'>
                    <p>ID : {id}</p>
                    <p>Name : {name}</p>
                    <p>Desc : {description}</p>
                    <p>Price : {price}</p>
                </div>
            </div>
        )
    }
}

// anfn
let mapStateToProps = (state) => {
    return {
        detail: state.shoeReducer.detail
    }
}

export default connect(mapStateToProps)(detailShoe);
