import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addToCartAction, seeDetail } from './redux/action/shoeAction'

class itemShoe extends Component {
    render() {
        let { image, name, price } = this.props.data
        return (
            <div className='col-3 p-3'>
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt='' />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <h2 className="card-text">{price}$</h2>
                    </div>
                    <div>
                        <button onClick={() => { this.props.handleChangeDetail(this.props.data) }} className='btn btn-secondary'>Xem chi tiết</button>
                        <button onClick={() => { this.props.handleAddToCart(this.props.data) }} className='btn btn-danger'>Chọn mua</button>
                    </div>
                </div>
            </div>
        )
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        handleChangeDetail: (shoe) => {
            dispatch(seeDetail(shoe));
        },

        handleAddToCart: (shoe) => {
            dispatch(addToCartAction(shoe))
        }
    }
}

export default connect(null, mapDispatchToProps)(itemShoe);

