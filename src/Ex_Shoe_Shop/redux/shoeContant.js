export const ADD_TO_CART = 'ADD_TO_CART';

export const SEE_DETAILS = 'SEE_DETAILS';

export const UP_DOWN_QTY = 'UP_DOWN_QTY';