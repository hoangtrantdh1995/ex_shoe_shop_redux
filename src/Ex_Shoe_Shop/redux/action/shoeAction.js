import { ADD_TO_CART, SEE_DETAILS, UP_DOWN_QTY } from "../shoeContant";

export const seeDetail = (value) => ({
    type: SEE_DETAILS,
    payload: value,
})
// () thay thế cho return
export const addToCartAction = (value) => ({
    type: ADD_TO_CART,
    payload: value,
})

export const upDownQty = (id, number) => ({
    type: UP_DOWN_QTY,
    payload: {
        idShoe: id,
        qty: number,
    }
})

