import React, { Component } from 'react'
import DetailShoe from './detailShoe'
import ListShoe from './listShoe'
import CartShoe from './cartShoe'


export default class Ex_Shoe_Shop extends Component {
    render() {
        return (
            <div className='container py-5'>
                <CartShoe />
                <ListShoe />
                <DetailShoe />

            </div>
        )
    }
}
