import { dataShoe } from "../dataShoe"
import { ADD_TO_CART, SEE_DETAILS, UP_DOWN_QTY } from "../redux/shoeContant";

let initialState = {
    shoeArr: dataShoe,
    detail: [],
    cart: [],
};

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            // create new array ~ spread operater
            let cloneCart = [...state.cart];
            // TH1 : thêm sp vào vào giỏi hàng dự vào id
            let index = cloneCart.findIndex((item) => item.id == action.payload.id)
            if (index == -1) {
                // dùng spread operater tạo ra obj mới dựa trên ojb cũ đã có
                let cartItem = { ...action.payload, qty: 1 }
                cloneCart.push(cartItem)
            } else {
                cloneCart[index].qty++;
            }
            // tạo ra ô nhớ mới và updated trên redux
            return { ...state, cart: cloneCart }
        }
        case UP_DOWN_QTY: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => item.id == action.payload.idShoe)
            if (index >= 0) {
                cloneCart[index].qty += action.payload.qty
            }
            cloneCart[index].qty == 0 && cloneCart.splice(index, 1)
            return { ...state, cart: cloneCart }
        }
        case SEE_DETAILS: {
            return { ...state, detail: action.payload }
        }
        default:
            return state;
    }
}