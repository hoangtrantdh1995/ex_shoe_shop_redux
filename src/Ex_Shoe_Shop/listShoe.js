import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './itemShoe'


class listShoe extends Component {
    renderListShoe = () => {
        return this.props.listShoe.map((item, index) => {
            return (<ItemShoe data={item} key={index} />)
        })
    }

    render() {
        return (
            <div className='row'>{this.renderListShoe()}</div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        listShoe: state.shoeReducer.shoeArr
    }
}

export default connect(mapStateToProps)(listShoe)
